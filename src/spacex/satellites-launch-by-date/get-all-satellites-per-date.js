const axios = require('axios');
const { API } = require('../config');

let satellitesLaunchesByDate;

/**
 * Fetches all satellites and transforms the data into the following object :
   {
      [year]: {
        [month]: {
          [day]: [starlink, ...]
        },
        ...
      }
   }
 * This structure allows to easily lookup satellites for a specific date without the need of filtering each time
 * @returns {Promise<*>}
 */
const getAllSatellitesPerDate = async () => {
  if (!satellitesLaunchesByDate) {
    const satellites = await axios.get(`${API}/starlink`);

    satellitesLaunchesByDate = satellites.data.reduce((acc, starlink) => {
      const [year, month, day] = starlink.spaceTrack.LAUNCH_DATE.split('-');

      acc[year] = acc[year] || {};
      acc[year][month] = acc[year][month] || {};
      acc[year][month][day] = acc[year][month][day] || [];

      acc[year][month][day].push(starlink);

      return acc;
    }, {});
  }

  return satellitesLaunchesByDate;
}

const resetCachedSatellites = () => satellitesLaunchesByDate = null;

module.exports = {
  getAllSatellitesPerDate,
  resetCachedSatellites
}
