const { getAllSatellitesPerDate } = require('./get-all-satellites-per-date');
const { isDateValid } = require('./utils/is-date-valid');

const numberToString = nb => nb <= 9 ? `0${nb}` : nb.toString();

const filterSatellitesLaunchedByDate = async date => {
  if (!isDateValid(date)) {
    throw new Error(
      `Date parameters ${JSON.stringify(date)} are invalid. Check that the year is correct, and if you have a day, you must have a month.`
    );
  }

  const satellitesPerDate = await getAllSatellitesPerDate();
  const { year, month, day } = date;
  satellitesPerDate[year] = satellitesPerDate[year] || {};

  if (!month) {
    return Object.values(satellitesPerDate[year]).reduce((acc, monthlySatellites) => {
      return [...Object.values(monthlySatellites).flat(), ...acc];
    }, []);
  }

  const monthString = numberToString(month);
  satellitesPerDate[year][monthString] = satellitesPerDate[year][monthString] || {};

  if (!day) {
    return Object.values(satellitesPerDate[year][monthString]).flat();
  }

  const dayString = numberToString(day);

  return satellitesPerDate[year][monthString][dayString] || [];
}

module.exports = {
  filterSatellitesLaunchedByDate,
}
