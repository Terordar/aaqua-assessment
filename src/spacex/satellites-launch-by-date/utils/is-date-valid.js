
const isDateValid = ({ year, month, day }) => {
  // The date must have a year and, if it has a day, it also needs to have a month
  if (!year || (day && !month)) {
    return false;
  }

  const dateString = year + (month ? `-${month}` : '') + (day ? `-${day}` : '');
  const date = new Date(dateString);

  // If date is valid
  return date instanceof Date && !isNaN(date);
}

module.exports = {
  isDateValid
}
