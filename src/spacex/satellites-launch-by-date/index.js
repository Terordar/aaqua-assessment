const { filterSatellitesLaunchedByDate } = require('./filter-satellites-launch-by-date');
const { year, month, day } = require('minimist')(process.argv.slice(2));

(async () => {
  const date = { year, month, day };

  try {
    const satellitesLaunchedByDate = await filterSatellitesLaunchedByDate(date);

    console.log(
      `Information of satellites launched for date ${JSON.stringify(date)}:`,
      JSON.stringify(satellitesLaunchedByDate, null, 2)
    );
  } catch (err) {
    console.error(`An error occurred when fetching satellites launched for date ${JSON.stringify(date)}`, err);
  }
})();
