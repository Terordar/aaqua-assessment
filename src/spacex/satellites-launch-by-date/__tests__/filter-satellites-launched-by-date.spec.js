const { filterSatellitesLaunchedByDate } = require('../filter-satellites-launch-by-date');
const { getAllSatellitesPerDate } = require('../get-all-satellites-per-date');
const { isDateValid } = require('../utils/is-date-valid');

jest.mock('../utils/is-date-valid', () => ({
  isDateValid: jest.fn(() => true)
}))

jest.mock('../get-all-satellites-per-date', () => ({
  getAllSatellitesPerDate: jest.fn()
}));

describe('filterSatellitesLaunchedByDate', () => {
  describe('When date parameters are valid', () => {
    const satellite1 = { id: '1' };
    const satellite2 = { id: '2' };
    const satellite3 = { id: '3' };
    const satellite4 = { id: '4' };
    const satellite5 = { id: '5' };
    const satellitesPerDate = {
      '2019': {
        '05': {
          '25': [satellite3],
          '30': [satellite5]
        },
        '06': {
          '01': [satellite4]
        }
      },
      '2020': {
        '05': {
          '07': [satellite1, satellite2]
        }
      }
    };

    getAllSatellitesPerDate.mockReturnValue(Promise.resolve(satellitesPerDate));

    it('returns all the satellites that were launched for a specific year', async () => {
      const res = await filterSatellitesLaunchedByDate({ year: 2019 });

      expect(res).toEqual([
        satellite4,
        satellite3,
        satellite5
      ]);
    });

    it('returns all the satellites that were launched for a specific year and month', async () => {
      const res = await filterSatellitesLaunchedByDate({ year: 2019, month: 5 });

      expect(res).toEqual([
        satellite3,
        satellite5
      ]);
    });

    it('returns all the satellites that were launched for a specific year, month and day', async () => {
      const res = await filterSatellitesLaunchedByDate({ year: 2020, month: 5, day: 7 });

      expect(res).toEqual([
        satellite1,
        satellite2
      ]);
    });

    it('returns an empty array if the no satellite was launched for a specific year', async () => {
      const res = await filterSatellitesLaunchedByDate({ year: 2021 });
      expect(res).toEqual([]);
    });

    it('returns an empty array if the no satellite was launched for a specific year and month', async () => {
      const res = await filterSatellitesLaunchedByDate({ year: 2019, month: 1 });
      expect(res).toEqual([]);
    });

    it('returns an empty array if the no satellite was launched for a specific year, month and day', async () => {
      const res = await filterSatellitesLaunchedByDate({ year: 2019, month: 5, day: 1 });
      expect(res).toEqual([]);
    });
  });

  describe('When the date parameters are not valid', () => {
    it('throws an error', async () => {
      isDateValid.mockReturnValue(false);

      await expect(filterSatellitesLaunchedByDate({ id: 1 })).rejects.toThrow(
        new Error(
          `Date parameters {"id":1} are invalid. Check that the year is correct, and if you have a day, you must have a month.`
        )
      );
    });
  });
});
