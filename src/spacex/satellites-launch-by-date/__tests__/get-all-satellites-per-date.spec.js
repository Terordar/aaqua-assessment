const axios = require('axios');

const { getAllSatellitesPerDate, resetCachedSatellites } = require('../get-all-satellites-per-date');
const { API } = require('../../config');

jest.mock('axios');

describe('getAllSatellitesPerDate', () => {
  afterEach(() => {
    resetCachedSatellites();
  });

  describe('When fetching satellites', () => {
    it('performs a GET /starlink request to fetch all satellites', async () => {
      axios.get.mockReturnValueOnce({ data: [] });

      await getAllSatellitesPerDate();

      expect(axios.get).toHaveBeenCalledWith(`${API}/starlink`);
    });

    it('returns the cached transformed response when called more than once', async () => {
      axios.get.mockReturnValueOnce({ data: [{ id: '1', spaceTrack: { LAUNCH_DATE: '2020-05-25' } }] });

      const res1 = await getAllSatellitesPerDate();
      const res2 = await getAllSatellitesPerDate();

      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(res1).toBe(res2); // Checking referential identity
    });

    it('returns the data with year, month and day keys for performant access', async () => {
      const satellite1 = { id: '1', spaceTrack: { LAUNCH_DATE: '2020-05-25' } };
      const satellite2 = { id: '2', spaceTrack: { LAUNCH_DATE: '2020-05-25' } };
      const satellite3 = { id: '3', spaceTrack: { LAUNCH_DATE: '2019-05-25' } };
      const satellite4 = { id: '4', spaceTrack: { LAUNCH_DATE: '2019-06-01' } };
      const data = [
        satellite1,
        satellite2,
        satellite3,
        satellite4,
      ];
      axios.get.mockReturnValueOnce({ data });

      const result = await getAllSatellitesPerDate();

      expect(result).toStrictEqual({
        '2019': {
          '05': {
            '25': [satellite3]
          },
          '06': {
            '01': [satellite4]
          }
        },
        '2020': {
          '05': {
            '25': [
              satellite1,
              satellite2
            ]
          }
        }
      });
    });
  });

  describe('Throws an error', () => {
    it('when the GET /starlink request failed', async () => {
      axios.get.mockImplementationOnce(() => Promise.reject(new Error('Error')));

      await expect(getAllSatellitesPerDate()).rejects.toThrow(
        new Error('Error'),
      );
    });
  });
});
