const axios = require('axios');
const { API } = require('../config');

const sortFailedLaunchByDateDesc = (failedLaunchA, failedLaunchB) => failedLaunchB.date_unix - failedLaunchA.date_unix;

const mapFailedLaunchNameAndReason = failedLaunch => ({ name: failedLaunch.name, failures: failedLaunch.failures.map(failure => failure.reason) });

const getFailedLaunches = async launchpadId => {
  const { data } = await axios.post(`${API}/launchpads/query`, {
    query: {
      _id: launchpadId,
    },
    options: {
      select: 'name launches',
      populate: [
        {
          path: 'launches'
        }
      ]
    }
  });

  const launchpad = data.docs[0];

  if (!launchpad) {
    throw new Error(`No launchpad was found for ID ${launchpadId}`);
  }

  const failedLaunches = launchpad.launches.filter(launch => launch.failures.length > 0);

  const all_failures = failedLaunches
    .sort(sortFailedLaunchByDateDesc)
    .map(mapFailedLaunchNameAndReason);

  return {
    all_failures,
    launchpad: launchpad.name
  }
}

module.exports = getFailedLaunches;
