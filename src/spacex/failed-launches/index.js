const { launchpadId } = require('minimist')(process.argv.slice(2));
const getFailedLaunches = require('./get-failed-launches');

if (!launchpadId) {
  console.error('No ID was passed as argument. You can pass the id as parameter with --launchpadId');
  process.exit();
}

(async () => {
  try {
    const launchpadFailures = await getFailedLaunches(launchpadId);

    console.log(
      `Information of failed launches for launchpad ID ${launchpadId} : `,
      JSON.stringify(launchpadFailures, null, 2)
    );
  } catch (err) {
    console.error(`An error occurred when fetching failed launches of launchpad ${launchpadId}`, err);
  }
})();
