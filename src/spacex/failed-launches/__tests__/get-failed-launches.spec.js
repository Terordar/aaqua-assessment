const axios = require('axios');
const getFailedLaunches = require('../get-failed-launches');
const { API } = require('../../config');

jest.mock('axios');

describe('getFailedLaunches', () => {
  const launchpadId = 'id1';

  it('Sends request POST /launchpads/query with the needed parameters to fetch launchpad and failures', async () => {
    axios.post.mockReturnValueOnce(Promise.resolve({ data: { docs: [{ name: 'L1', launches: [] }] } }));

    await getFailedLaunches(launchpadId);

    expect(axios.post).toHaveBeenCalledWith(`${API}/launchpads/query`, {
      query: {
        _id: launchpadId,
      },
      options: {
        select: 'name launches',
        populate: [
          {
            path: 'launches'
          }
        ]
      }
    });
  });

  describe('When a launchpad has failed launches', () => {
    it('returns the name of the launchpad and information about failed launches', async () => {
      const data = require('./launchpad-data/with-failures-mock.json');
      const expectedResult = require('./launchpad-data/expected-result.json');
      axios.post.mockReturnValueOnce(Promise.resolve({ data }))

      const result = await getFailedLaunches(launchpadId);

      expect(result).toStrictEqual(expectedResult);
    });

    it('sorts the failed launches by desc date (most recent first, oldest last)', async () => {
      const data = require('./launchpad-data/with-failures-mock.json');
      axios.post.mockReturnValueOnce(Promise.resolve({ data }))

      const result = await getFailedLaunches(launchpadId);

      expect(result.all_failures[0].name).toBe('Trailblazer');
      expect(result.all_failures[result.all_failures.length - 1].name).toBe('FalconSat');
    });
  });

  describe('When a launchpad has no failures', () => {
    it('returns the launchpad name with empty failures', async () => {
      const data = require('./launchpad-data/without-failures-mock.json');
      const expectedResult = {
        launchpad: 'VAFB SLC 4E',
        all_failures: []
      };
      axios.post.mockReturnValueOnce(Promise.resolve({ data }));

      const result = await getFailedLaunches(launchpadId);

      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe('Throws an error', () => {
    it('when the POST /launchpads/query fails', async () => {
      axios.post.mockImplementationOnce(() => Promise.reject(new Error('Error')));

      await expect(getFailedLaunches(launchpadId)).rejects.toThrow(
        new Error('Error'),
      );
    });

    it('when no launchpad was found', async () => {
      axios.post.mockReturnValueOnce(Promise.resolve({ data: { docs: [] } }));

      await expect(getFailedLaunches(launchpadId)).rejects.toThrow(
        new Error(`No launchpad was found for ID ${launchpadId}`),
      );
    });
  });
});
