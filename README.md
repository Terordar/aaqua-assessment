# Aaqua Assignment

The project was built using :
* Basic HTML/CSS for the dashboard
* Vanilla JS
* [_Jest_](https://www.npmjs.com/package/jest) for unit testing
* [_Axios_](https://www.npmjs.com/package/axios) to make HTTP requests
* [_Minimist_](https://www.npmjs.com/package/minimist) to handle arguments that are passed when running a script

## Prerequisites

* Install [Node.js](https://nodejs.org/) (minimum version 12 required) which includes [Node Package Manager](https://www.npmjs.com/get-npm)

* Clone the project : ```git clone git@gitlab.com:Terordar/aaqua-assessment.git```

* Install the dependencies : ```npm i```

## Assignments
### Dashboard
You can find a live version of the dashboard on the following URL : https://aaqua-dashboard.web.app

The code for the dashboard can be found under ```src/dasboard```

You can open ```index.html``` on your local environment to preview the dashboard. 

### Functions
#### Failed launches for a launchpad
The code of this function can be found under ```src/spacex/failed-launches```

This function accepts an `id` of a `launchpad` as an argument, and returns information about failed `launches` (desc).

Run the function by using the following command :

```node src/spacex/failed-launches/index.js --launchpadId=5e9e4502f5090995de566f86```

The value of ```launchpadId``` can be changed in the command.

#### Look up all starlink satellites launched on a specific date
The code of this function can be found under ```src/spacex/satellites-launch-by-date```

Two functions exist for this feature :
* ```getAllSatellitesPerDate``` : returns an object that maps the year, month and day with the satellites launched. This object has the following format :
    ``` 
  {
      [year]: {
        [month]: {
          [day]: [starlink, ...]
        },
        ...
      }
  }
  ```
  The function will cache the transformed data. You can call it multiple times in a script and it will only make one HTTP request to get the data.
* ```filterSatellitesLaunchedByDate``` : returns all the satellites launched on a specific year, month and/or date.
  
  This function uses  ```getAllSatellitesPerDate```, to easily retrieve the data by accessing the specific date. 

  Run the function by using the following command :
  
  ```node  src/spacex/satellites-launch-by-date/index.js --year=2020 --month=11 --day=25```

  The values of ```year```, ```month``` and ```day``` can be changed in the command. You can also omit the ```month``` and/or ```day```.

#### Unit tests
You can run the unit tests for the two functions above with the following command :

```npm run test```
